import react, { Component } from 'react';
import { View, StatusBar, Text, TextInput, FlatList} from 'react-native';


let database = [

  {english: 'love', indonesia: 'Rasanya begitu menyedihkan dan menyenangkan ketika waktuku untuk tertidur telah hilang hanya untuk memikirkanmu'},
  {english: 'sad', indonesia: 'Hal yang tak akan pernah bisa aku sesali adalah saat aku patah hati karenamu'},
  {english: 'cry', indonesia: 'Sungguh terpatri dalam diri ketika kamu tak terganti, tak bisa orang lain hanya kamu satu-satunya'},
  {english: 'recall the past', indonesia: 'Seperti lagu anak, ketika melihat kebun akan terlihat banyak bunga sedangkan saat melihatmu hatiku terus menerus merasa berbunga-bunga'},
  {english: 'in a relationship', indonesia: 'Aku hanya berharap bahwa tak mengapa antartika memiliki jarak yang sangat jauh sedangkan antarkita jangan pernah sampai berjauhan'},
  {english: 'miss', indonesia: 'Tak mengapa jika jaringan seringkali menghilang, asalkan kamu jangan hilang'},
  {english: 'thought', indonesia: 'Makan hati, tak pernah begitu menyakitkan asalkan bersamamu'},
]

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
    Text: '',
    data: database
   }; 
  }

  search = () => {
    let data = database;


    data = data.filter( item => item.indonesia.toLocaleLowerCase(). includes(this.state.text.toLocaleLowerCase()));
    this.setState({
      data: data
    })
  }

  render() {
    return(
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#BBDEFB" barstyle="light-content" />

        <View style={{pedding: 20, backgroundColor: '#304FFE'}}>
          <Text style={{textAlign: 'center', color: '#F48FB1',fontTweight: 'bold' }}> KATA-KATA BIKIN BAPER</Text>
        </View>

        <TextInput
          style={{ hight: 40, borderColor: 'gray', borderWidth: 1, paddingLeft: 10, marginVertical: 20, marginHorizontal: 10, borderRadius: 50}}
          onChangeText={text =>this.onChangeText ({text: text})}
          value={this.state.Text}
          placeholder='masukkan kata kunci'
          onKeyPress={() => this.search()}
        />

        <FlatList
          data={this.state.data}
          renderItem={({ item }) => 
            <View style={{borderWidth: 1, borderRadius: 3, marginVertical: 10, marginHorizontal: 20, padding: 10}}>
              <Text style={{fontSize: 18, fontWeight: 'bold'}}>{item.english}</Text>
              <Text style={{fontSize: 16, marginTop: 5}}>{item.indonesia}</Text>
            </View>
          }
          keyExractor={item => item.indonesia}

        
        />
        
      </View>
    );
  }
}

export default App;
